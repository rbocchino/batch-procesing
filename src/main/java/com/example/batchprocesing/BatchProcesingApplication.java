package com.example.batchprocesing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BatchProcesingApplication {

	public static void main(String[] args) {
		SpringApplication.run(BatchProcesingApplication.class, args);
	}

}
