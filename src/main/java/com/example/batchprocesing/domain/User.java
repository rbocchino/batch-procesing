package com.example.batchprocesing.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="user")
public class User {

	@Id
	@Column(name = "id")
	private Integer id;
	
	@Column(name = "name")
	private String fullName;
	
	@Column(name = "age")
	private Long age;
	
	@Column(name = "email")
	private String email;

	public String getFullName() {
		return this.fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public Long getAge() {
		return this.age;
	}

	public void setAge(Long age) {
		this.age = age;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "User [fullName=" + this.fullName + ", age=" + this.age + ", email=" + this.email + "]";
	}
	
	
}
