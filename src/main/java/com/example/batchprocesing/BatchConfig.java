package com.example.batchprocesing;

import javax.persistence.EntityManagerFactory;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.JpaPagingItemReader;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor;
import org.springframework.batch.item.file.transform.DelimitedLineAggregator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;

import com.example.batchprocesing.domain.User;

@Configuration
@EnableBatchProcessing
public class BatchConfig {

	@Bean
	public ItemReader<User> reader(EntityManagerFactory entityManagerFactory) throws Exception {
		String jpqlQuery = "select u from User u";
		JpaPagingItemReader<User> reader = new JpaPagingItemReader<>();
		reader.setQueryString(jpqlQuery);
		reader.setEntityManagerFactory(entityManagerFactory);
		reader.setPageSize(3);
		reader.afterPropertiesSet();
		reader.setSaveState(true);
		return reader;
	}

	@Bean
	public ItemWriter<User> writer() {
		FlatFileItemWriter<User> writer = new FlatFileItemWriter<>();
		Resource outputResource = new FileSystemResource("C:\\Users\\profesor\\outputData.csv");
		writer.setResource(outputResource);

		writer.setLineAggregator(new DelimitedLineAggregator<User>() {
			{
				setDelimiter(",");
				setFieldExtractor(new BeanWrapperFieldExtractor<User>() {
					{
						setNames(new String[] { "fullName", "age", "email" });
					}
				});
			}
		});
		return writer;
	}

	@Bean
	public Step step(StepBuilderFactory stepBuilderFactory, ItemReader<User> reader, ItemWriter<User> writer ) {
		return stepBuilderFactory.get("load-csv-user")
				.<User,User>chunk(100)
				.reader(reader)
				.writer(writer)
				.build();
	}

	@Bean
	public Job job(JobBuilderFactory jobBuilderFactory, Step step) {
		return jobBuilderFactory.get("etl-2").incrementer(new RunIdIncrementer()).start(step).build();
	}
}
